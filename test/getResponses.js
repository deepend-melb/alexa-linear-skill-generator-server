const view = require("./view.json");

const x = Object.values(view.en.translation.Response)
  .map(i => i.ask)
  .filter(i => i !== undefined)
  .map(i => i[0]);

console.log(JSON.stringify(x.join("\n")));
