const bucket = require("../S3Storage");
const scripts = require("./scripts.json");
const fs = require("fs");

async function init() {
    const scripts = await bucket.readScripts();
    fs.writeFileSync("./scripts.json", JSON.stringify(scripts, null, 2));
   

}

init();
