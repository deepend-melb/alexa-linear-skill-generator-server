const bucket = require("../S3Storage");

async function init() {

    const scripts = require('./scripts.json')
    await bucket.writeScripts(scripts)

}

init();
