const helpers = require("../helpers");

const skillName = "testingExample";

helpers
  .createSkill(skillName)
  .then(res => {
    console.log(res);
  })
  .catch(err => console.log(err));
