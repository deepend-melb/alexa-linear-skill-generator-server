const exec = require("child_process").exec;
const fs = require("fs");
const bucket = require("./S3Storage");

const execute = command => {
  return new Promise((resolve, reject) => {
    const script = exec(command);
    console.log({ executingCommand: command });

    script.stdout.on("data", data => {
      console.log(data);
    });

    // This is disabled because when ask-cli asks for confirmation is a warn so the promise gets rejected:
    script.stderr.on("data", err => {
      console.log(err);
    });

    script.on("close", () => {
      resolve("Skill Created");
    });
  });
};

// This is done just to format the sample utterances according to ask_cli requirements.
const parseToValidUtteranceSample = str => {
  return str
    .replace(/\,+/g, " ")
    .replace(/\?+/g, " ")
    .replace(/\!+/g, " ")
    .replace(/{+/g, "") // getting rid of slots
    .replace(/}+/g, "") // getting rid of slots
    .replace(/\n+/g, "") // getting rid of enters
    .replace(/@+/g, "")
    .replace(/\"+/g, "")
    .replace(/[0-9]/g, "")
    .replace(/\./g, "");
};

exports.injectCode = ({ skillName, invocationName, script }) => {
  return new Promise(async resolve => {
    // const { responses, reprompts } = script;
    const modelPath = `./${skillName}-prototype/models/en-AU.json`;
    const model = require(modelPath);
    model.interactionModel.languageModel.invocationName = invocationName;

    // Replacing universal intent samples with responses. This is done to potentially avoid alexa not recognizing users input.
    // const { intents } = model.interactionModel.languageModel;
    // const UniversalIntentIndex = intents
    //   .map(i => i.name)
    //   .indexOf("UniversalIntent");

    // model.interactionModel.languageModel.intents[
    //   UniversalIntentIndex
    // ].samples = responses.map(res => parseToValidUtteranceSample(res));

    fs.writeFileSync(modelPath, JSON.stringify(model, null, 2));

    // Injecting skillName to lambda
    const skillInfoPath = `./${skillName}-prototype/lambda/custom/skillInfo.json`;
    const skillInfo = {
      skillName
    };
    fs.writeFileSync(skillInfoPath, JSON.stringify(skillInfo));

    // Adding default scripts to bucket object.
    const scriptMem = await bucket.readScripts();
    scriptMem[`${skillName}`] = scriptMem["default"];
    bucket.writeScripts(scriptMem);

    resolve("done");
  });
};

// TODO: when creating a skill check if the skill exist in the bucket, do not allow duplicates as skillname is the main id.
// TODO: or we could allow duplicates and just overwrite the script.
exports.createSkill = ({ skillName }) => {
  return new Promise(async (resolve, reject) => {
    // If folder exist remove it, then create skill.
    const script = `
    SKILL_FOLDER=${skillName}-prototype;
    if [ -d "$SKILL_FOLDER" ]; then rm -Rf $SKILL_FOLDER; fi
    yes | ask new --url https://diegorodriguez8686@bitbucket.org/deepend-melb/linear-alexa-skill-template.git --skill-name $SKILL_FOLDER
    `;

    // cloning skill
    const scriptResponse = await execute(script).catch(err => {
      reject({ err });
    });

    resolve(scriptResponse);
  });
};

exports.deploy = ({ skillName }) => {
  return new Promise(async (resolve, reject) => {
    // // Deploying skill
    const deployScript = `
      cd ${skillName}-prototype/ && ask deploy
    `;

    await execute(deployScript).catch(err => {
      reject({ err });
    });

    // And deleting the folder after deploying
    const cleanUpScript = `rm -fr ${skillName}-prototype/`;

    await execute(cleanUpScript).catch(err => {
      reject({ err });
    });

    resolve("done");
  });
};
