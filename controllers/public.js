const express = require("express");
const helpers = require("../helpers");
const router = new express.Router();

router.get("/", (req, res) => {
  return res.send("api working");
});

const validate = (req, res, next) => {
  const { skillName, invocationName } = req.body;
  if (!skillName) {
    return res.status(400).send("No skillName was passed");
  }
  if (!invocationName) {
    return res.status(400).send("No invocationName was passed");
  }
  // Replacing skillName spaces with dashes, otherwise the command will break.
  req.body.skillName = req.body.skillName.replace(/\s+/g, "-");
  next();
};

router.post("/create-alexa-skill", validate, async (req, res) => {
  const { skillName } = req.body;
  console.log("Creating Skill");
  const response = await helpers.createSkill({ skillName }).catch(err => {
    console.log({ err });
    // return res.status(400).send(err);
  });

  console.log(response);
  return res.send(`Alexa Skill Created`);
});

router.post("/inject-linear-skill-code", validate, async (req, res) => {
  const { skillName, invocationName, script } = req.body;

  await helpers.injectCode({ skillName, invocationName, script }).catch(err => {
    return res.status(400).send(err);
  });

  return res.send(`Alexa Skill ${skillName} Updated`);
});

router.post("/deploy-linear-skill", validate, async (req, res) => {
  const { skillName } = req.body;

  await helpers.deploy({ skillName }).catch(err => {
    return res.status(400).send(err);
  });

  return res.send(`Alexa Skill ${skillName} Updated`);
});

module.exports = router;
