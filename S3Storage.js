const S3 = require("aws-sdk/clients/s3");
const axios = require("axios");
const s3 = new S3();

// scripts is an object
exports.writeScripts = scripts => {
  return new Promise((resolve, reject) => {
    var params = {
      Bucket: "linear-skill-generator",
      Key: "scripts.json",
      Body: JSON.stringify(scripts),
      ACL: "public-read",
      ContentType: "application/json"
    };

    s3.putObject(params, function(err, data) {
      if (err) reject(err);
      else resolve(data);
    });
  });
};

exports.readScripts = () => {
  return new Promise((resolve, reject) => {
    axios
      .get("https://linear-skill-generator.s3.amazonaws.com/scripts.json")
      .then(response => {
        const scripts = response.data;
        resolve(scripts);
      });
  });
};
