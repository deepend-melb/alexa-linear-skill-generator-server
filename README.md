### Deployment
This server was deployed in a micro.T2 EC2 instance:

In order to set up the same configuration you will need to do the following in the instance:
* Install nvm
* Install node 10.12
* Install python 2.7 
* Install ask cli 
* Run `ask init --no-browser`, follow instructions to add ask and aws credentials. 
* Clone this repo in the root folder of the instance. 
* npm install
* npm start
* set up pm2, so the express app runs in the background even if the instance reboots
  * stop all node instances `killall -9 node`
  * `npm install pm2 -g`
  * start server with pm2 eg. `pm2 start index.js`
  * run the command logged by `pm2 startup`, this is to restart pm2 when instance reboots
  * finally run `pm2 save`

* Add tcp custom rule in AWS EC2 console to allow port 5000 to be accessed.

After doing all this you can access the server with 
`publicDNS:port`

For example 
`http://ec2-XX-XXX-XXX-XXX.compute-X.amazonaws.com:5000`


### Setting up https
We need to set up https in the server so we dont get the `Mixed content error` To do this we need to configure `nginx`,  you can follow this guides:
https://medium.com/@nishankjaintdk/setting-up-a-node-js-app-on-a-linux-ami-on-an-aws-ec2-instance-with-nginx-59cbc1bcc68c
<!-- https://www.digitalocean.com/community/tutorials/how-to-create-an-ssl-certificate-on-nginx-for-ubuntu-14-04 -->

A Https endpoint can be easily achieved with AWS CloudFront, just need to point to the ec2 public dns
https://stackoverflow.com/questions/33137792/cloudfront-and-ec2


Currently the cloudfront url is set as env variable in the Netlify account.
