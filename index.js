const express = require("express");
const cors = require("cors");

const app = express();
const port = process.env.PORT || 5000;
//by doing this here we are telling express to use express.json() in all the app.
app.use(express.json());
app.use(cors());
//we are telling app to use everything in the controllers folder.
app.use(require("./controllers"));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.listen(port, () => {
  console.log(`listening on ${port}`);
});
